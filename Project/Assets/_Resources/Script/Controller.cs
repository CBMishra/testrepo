﻿using UnityEngine;
using LitJson;
using System;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public delegate void myDelegate(string jsonString,GameObject _headerText,GameObject _choicesText);

    public event myDelegate myEvent;

    public GameObject _headerText, _choicesText;

    [System.Serializable]
    public class MyJson
    {
        public string _question;
        public ArrayList _choices;
        public ArrayList _votes;
    }

    void Start()
    {
       
        string path = Application.streamingAssetsPath + "/table.json";
        path = File.ReadAllText(path);

        myEvent += Processjson;
        myEvent(path, _headerText, _choicesText);
    }

    private void Processjson(string jsonString, GameObject _headerText, GameObject _choicesText)
    {
        JsonData jsonvalue = JsonMapper.ToObject(jsonString);

        MyJson myJson;
        myJson = new MyJson();      

        myJson._question = jsonvalue["question"].ToString();
        _headerText.GetComponent<Text>().text = myJson._question;


        myJson._choices = new ArrayList();
        myJson._votes = new ArrayList();

        for (int i = 0; i < jsonvalue["choices"].Count; i++)
        {
            _choicesText.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = jsonvalue["choices"][i]["choice"].ToString();
            _choicesText.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = jsonvalue["choices"][i]["votes"].ToString();
        }    
    }
}
